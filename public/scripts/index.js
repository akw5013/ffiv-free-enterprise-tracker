const numImages = {
  "Cecil": 4,
  "Rydia": 4,
  "Default": 2
}
const imageFolder = "./assets/images/"
const images = [
  "Cecil",
  "Rydia",
  "Rosa",
  "Palom",
  "Porom",
  "Yang",
  "FuSoYa",
  "Kain",
  "Edge",
  "Cid",
  "Edward",
  "Tellah",
  "TheCrystal",
  "Pass",
  "Hook",
  "DarkCrystal",
  "EarthCrystal",
  "TwinHarp",
  "Package",
  "Sandruby",
  "BaronKey",
  "MagmaKey",
  "TowerKey",
  "LucaKey",
  "Adamant",
  "LegendSword",
  "Pan",
  "Spoon",
  "PinkTail",
  "RatTail",
  "MistDragon"
]
let currentImageMap = { }

function init() {
  if(currentImageMap.length === undefined) {
    for(const name of images) {
      currentImageMap[name] = 1
    }
  }
  if(locationStatus.length === undefined) {
    for(const name of locations) {
      locationStatus[name] = "Unavailable"
    }
    locationStatus["Fabul"] = "Available"
    locationStatus["Ordeals"] = "Available"
    locationStatus["AntlionCave"] = "Available"
  }
  updateFlags("flags")
}

function getNextImageName(imageName, imageRoot) {
  const currentImageNum = currentImageMap[imageName]
  const maxImages = numImages[imageName] || numImages["Default"]    
  let current = currentImageNum + 1
  if(current > maxImages) {
    current = 1
  }
  currentImageMap[imageName] = current
  //hacky crap i should fix
  if(imageRoot === "Items" || imageRoot === "Bosses") {
    updateLocationRequirements(imageName)
    checkForGoMode()
  }
  return `${imageFolder}${imageRoot}/${imageName}_${current}.png`
}

const locations = [
  "AdamantGrotto",
  "AntlionCave",
  "BabilTower",
  "Baron",
  "CaveMagnes",
  "DwarfCave",
  "Eblan",
  "Fabul",
  "LandOfSummonedMonsters",
  "LowerBabil",
  "Mist",
  "Ordeals",
  "SealedCave",
  "SylphCave",
  "Zot"
]

let locationStatus = { }

function getCompletedImage(imageName, imageRoot, toggle = true) {
  //console.log(`Getting image for ${imageName} with root ${imageRoot}`)
  const status = locationStatus[imageName]
  let newStatus = status
  if(toggle) {    
    switch(status) {
      case "Available":
        newStatus = "Completed"
        break
      case "Completed":
        newStatus = "Available"
        break
    }
    locationStatus[imageName] = newStatus
    //Check to make sure it's actually available
    newStatus = getLocationStatus(imageName)
    locationStatus[imageName] = newStatus
  }
  return `${imageFolder}${imageRoot}/${imageName}_${newStatus}.png`
}

let kFlag = 0;
let bFlag = 0;
let nFlag = 0;

function updateFlags(id) {
  const flagSet = document.getElementById(id).value
  if(flagSet) {
    kFlag = findFlagValue(flagSet, "K")
    bFlag = findFlagValue(flagSet, "B")
    nFlag = findFlagValue(flagSet, "N") 
  }
  const shouldHideNFLLocations = (nFlag === 0 || nFlag === undefined)
  document.getElementById("MistDragon").hidden = shouldHideNFLLocations
  document.getElementById("Mist").hidden = shouldHideNFLLocations
}

function findFlagValue(flags, flag) {
  const regex = new RegExp(`(${flag})(\\d?)`)
  const flagVal = flags.match(regex)
  if(flagVal && flagVal[1]) {
    return flagVal[2] ? flagVal[2] : 1
  } else {
    return 0
  }
}

const locationRequirements = {
  "AdamantGrotto": { "required": [ "Hook", "RatTail" ]},
  "AntlionCave" : { },
  "BabilTower": { "required": ["TowerKey"], "optional": [ "MagmaKey", "Hook" ] },
  "Baron": { "required": ["BaronKey"] },
  "CaveMagnes": { "required": ["TwinHarp"] },
  "DwarfCave": { "optional": ["MagmaKey", "Hook"] },
  "Eblan": { "required": [ "Hook" ] },
  "Fabul": { },
  "LandOfSummonedMonsters": { "optional": [ "MagmaKey", "Hook"] },
  "LowerBabil": { "optional" : [ "MagmaKey", "Hook" ] },
  "Mist": { "required": ["MistDragon"] },
  "Ordeals": { },
  "SealedCave" : { "required" : ["LucaKey"], "optional": ["MagmaKey", "Hook"] },
  "SylphCave": { "optional": ["MagmaKey", "Hook"] },
  "Zot": { "required": ["EarthCrystal"] }
}

let currentItems = []
function updateLocationRequirements(item) {
  if(currentItems.indexOf(item) >= 0) {
    currentItems.splice(currentItems.indexOf(item), 1)
  } else {
    currentItems.push(item)
  }
  //console.log("Updating requirements because of " + item)
  for(const location of locations) {
    let status = getLocationStatus(location)
    locationStatus[location] = status
  }
  updateLocationImages()
}

function getLocationStatus(location) {
  if(locationStatus[location] === "Completed") {
    return "Completed"
  }  
  //console.log("Finding requirements for " + location)
  const requirements = locationRequirements[location]    
  let hasRequiredItems = true
  let hasAnOptionalItem = false
  let status = "Available"
  if(requirements) {
    if(requirements.required) {
      for(const required in requirements.required) {
        const itemName = requirements.required[required]
        if(currentItems.indexOf(itemName) < 0) {
          hasRequiredItems = false
          break
        }
      }
    }
    let hasAnOptionalItem = false
    if(requirements.optional) {             
      for(const optional in requirements.optional) {
        const itemName = requirements.optional[optional]   
        if(currentItems.indexOf(itemName) >= 0) {
          hasAnOptionalItem = true
          break
        }
      }
    }
    if(hasRequiredItems && (hasAnOptionalItem || !requirements.optional)) {
      status = "Available"
    } else {
      status = "Unavailable"
    }
  }
  return status
}

function updateLocationImages() {
  for(const location of locations) {
    document.getElementById(location).src = getCompletedImage(location, "Locations/Small", false)
  }
}

function checkForGoMode() {
  const hasCrystal = currentItems.indexOf("TheCrystal") >= 0
  const hasPass = currentItems.indexOf("Pass") >= 0
  const hasDarkCrystal = currentItems.indexOf("DarkCrystal") >= 0

  const isAlmostMode = (hasPass || hasDarkCrystal || hasCrystal)
  const isGoMode = hasCrystal && (hasPass || hasDarkCrystal)
  if(isGoMode) {
    document.getElementById("GoMode").src = "./assets/images/Misc/Go_Mode.png"    
  } else if (isAlmostMode) {
    document.getElementById("GoMode").src = "./assets/images/Misc/Almost_Mode.png"    
  } else {
    document.getElementById("GoMode").src = "./assets/images/Misc/Search_Mode.png"
  }
}