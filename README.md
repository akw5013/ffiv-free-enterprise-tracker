This is a item/location tracker build for the Final Fantasy 4 Free Enterprise ROMHack (http://ff4-free-enterprise.com/)

It's based off of some existing trackers (http://schala-kitty.net/ff4fe-tracker/)

Merge Requests and suggestions are welcome :)

This is built using vanilla JS and HTML, and auto published to GitLab pages. (https://akw5013.gitlab.io/ffiv-free-enterprise-tracker/)


To Use:
- Click each item as you acquire it, the locations available to you will light up
- Click each location as you finish it to mark it completed

Future Updates:
- Implement "Go Mode" button
- Implement Mist only for N1/N2 flags (Display Edward in Bed otherwise)
- Implement Boss item locations (for K2 Flags)
- Implement Trapped Chest locations (For K4 Flags)
- Implement routing potential based on B flags (Golbez can't block Underworld for ex, so finding him at Eblan means Magma has to be in overworld)